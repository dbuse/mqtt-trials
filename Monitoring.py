import time
import random
import pandas as pd
import threading
from flask import Flask, Response, jsonify, request, send_from_directory
from dcps import AimTTiPLP, Keithley2400
from nested_dict import nested_dict
import sys
import signal
import argparse
from datetime import datetime

import requests
from time import sleep

import parser 
import encodings
import json 
import fmt 
import log
import net 
import http 

import paho.mqtt.client as mqtt_client
import paho.mqtt.client as mqtt

from mqtt_broker import telegraf
from influxdb import InfluxDBClient
from prometheus_client import CollectorRegistry, Gauge, push_to_gateway
from influxdb_client import WritePrecision, InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS

# Create Flask app
app = Flask(__name__)

config = {
    'PowerSupply': [
        {'hostname': 'example1', 'port': 1234},
        {'hostname': 'example2', 'port': 5678}
    ],
    'BiasVoltage': [
        {'hostname': 'example3', 'port': 9012, 'gpib': 3},
        {'hostname': 'example4', 'port': 3456, 'gpib': 5}
    ]
}


broker = 'broker.emqx.io'
port = 1883
topic = "python/mqtt"

# Generate a Client ID with the publish prefix.
client_id = f'publish-{random.randint(0, 1000)}'

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print(f"Failed to connect, return code {rc}")

    client = mqtt.Client(client_id)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


# Define global variables
modules = 2
shutting_down = False
instruments = nested_dict()
instruments_key = threading.Lock()

def initialize_instruments():
    for i in range(modules):
        instruments_key.acquire()
        instruments[i]['tti']['dev'] = AimTTiPLP('TCPIP::' + config['PowerSupply'][i]['hostname'] +
                                                 '::' + str(config['PowerSupply'][i]['port']) + '::SOCKET')
        instruments[i]['keithley']['dev'] = Keithley2400('TCPIP::' + config['BiasVoltage'][i]['hostname'] +
                                                          '::' + str(config['BiasVoltage'][i]['port']) + '::SOCKET',
                                                          config['BiasVoltage'][i]['gpib'])
        instruments_key.release()

        instruments_key.acquire()
        for instrument in instruments[i]:
            try:
                instruments[i][instrument]['dev'].open()
                instruments[i][instrument]['open'] = True
            except:
                instruments[i][instrument]['open'] = False
                print(f"Failed to open {instrument} on module {i}")
        instruments_key.release()

def read_tti_func():
    while not shutting_down:
        instruments_key.acquire()
        for i in range(modules):
            try:
                if not instruments[i]['tti']['open']:
                    raise Exception("TTi not open")
                instruments[i]['tti']['meas'][1]['I'] = instruments[i]['tti']['dev'].measureCurrent(1)
                instruments[i]['tti']['meas'][1]['V'] = instruments[i]['tti']['dev'].measureVoltage(1)
                instruments[i]['tti']['meas'][2]['I'] = instruments[i]['tti']['dev'].measureCurrent(2)
                instruments[i]['tti']['meas'][2]['V'] = instruments[i]['tti']['dev'].measureVoltage(2)
            except Exception as e:
                instruments[i]['tti']['meas'][1]['I'] = "N/A"
                instruments[i]['tti']['meas'][1]['V'] = "N/A"
                instruments[i]['tti']['meas'][2]['I'] = "N/A"
                instruments[i]['tti']['meas'][2]['V'] = "N/A"
            mqtt_message = str(instruments[i]['tti']['meas'].to_dict())
            mqtt_client.publish(f"{i}tti", mqtt_message)
        instruments_key.release()
        time.sleep(0.2)

def read_keithley_func():
    while not shutting_down:
        instruments_key.acquire()
        for i in range(modules):
            try:
                if not instruments[i]['keithley']['open']:
                    raise Exception("Keithley not open")
                if not instruments[i]['keithley']['dev'].isOutputOn(1):
                    raise Exception("Keithley output not on")
                instruments[i]['keithley']['meas'][1]['V'] = instruments[i]['keithley']['dev'].measureVoltage(1)
                time.sleep(0.5)
                instruments[i]['keithley']['meas'][1]['I'] = instruments[i]['keithley']['dev'].measureCurrent(1)
            except Exception as e:
                instruments[i]['keithley']['meas'][1]['V'] = "N/A"
                instruments[i]['keithley']['meas'][1]['I'] = "N/A"
            mqtt_message = str(instruments[i]['keithley']['meas'].to_dict())
            mqtt_client.publish(f"{i}keithley", mqtt_message)
        instruments_key.release()
        time.sleep(2)


tti_measurement_thread = threading.Thread(target=read_tti_func)
tti_measurement_thread.start()

keithley_measurement_thread = threading.Thread(target=read_keithley_func)
keithley_measurement_thread.start()

app = Flask(__name__)

@app.route("/")
def base():
    return send_from_directory('../client/public', 'index.html')

@app.route("/<path:path>")
def home(path):
    return send_from_directory('../client/public', path)


@app.route('/monitor/<module>/<instrument>/<channel>', methods=['GET'])
def monitor_ps(module, instrument, channel):
    module = int(module)
    channel = int(channel)

    data = {}
    with instruments_key:
        data['V'] = instruments[module][instrument]['meas'][channel]['V']
        data['I'] = instruments[module][instrument]['meas'][channel]['I']

    return jsonify(data)


@app.route('/turn_on', methods=['POST'])
def turn_on():
    data = request.get_json()

    module = int(data["module"])
    instrument = data["instrument"]
    channel = int(data["channel"])

    with instruments_key:
        if not instruments[module][instrument]['open']:
            return Response("Instrument not open", status=500)

        instruments[module][instrument]['dev'].outputOn(channel)
        return Response("Turn on complete", status=200)


@app.route('/turn_off', methods=['POST'])
def turn_off():
    data = request.get_json()

    module = int(data["module"])
    instrument = data["instrument"]
    channel = int(data["channel"])

    with instruments_key:
        if not instruments[module][instrument]['open']:
            return Response("Instrument not open", status=500)

        instruments[module][instrument]['dev'].outputOff(channel)
        return Response("Turn off complete", status=200)


def shutdown_handler(signal, frame):
    global shutting_down

        shutting_down = True
    print("Shutting down...")
    tti_measurement_thread.join()
    keithley_measurement_thread.join()

    for i in range(modules):
        for instrument in instruments[i]:
            if instruments[i][instrument]['open']:
                instruments[i][instrument]['dev'].setLocal()
                instruments[i][instrument]['dev'].close()

    sys.exit(0)


signal.signal(signal.SIGINT, shutdown_handler)

# InfluxDB configuration
influxdb_host = "localhost"
influxdb_port = 8086
influxdb_token = "your-influxdb-token"
influxdb_org = "your-org"
influxdb_bucket = "your-bucket"

# MQTT configuration
mqtt_broker = "broker.emqx.io"
mqtt_port = 1883
mqtt_topic = "python/mqtt"

# Prometheus configuration
prometheus_gateway = "http://localhost:9091"
prometheus_job = "your-job"
prometheus_instance = "your-instance"

# Telegraf configuration
telegraf_host = "localhost"
telegraf_port = 8094
telegraf_measurement = "sensor_data"

# Grafana configuration
grafana_url = "http://localhost:3000"
grafana_api_key = "your-api-key"
grafana_dashboard_id = "your-dashboard-id"


def generate_dataframe():
    # Example DataFrame
    data = {
        'Temperature': [25.1, 26.3, 24.8, 25.6, 26.2],
        'Humidity': [50.5, 48.9, 51.2, 49.8, 52.1]
    }
    df = pd.DataFrame(data)
    return df

#Write data to InfluxDB
def write_to_influxdb(temperature, humidity): #write_to_influxdb means telegraf
    while not shutting_down:
        df = generate_dataframe()
        json_body = df.to_dict(orient='records')
        influx_client.write_points(json_body)
        time.sleep(5)
 client = InfluxDBClient(url=f"http://{influxdb_host}:{influxdb_port}", token=influxdb_token, org=influxdb_org)
    write_api = client.write_api(write_options=SYNCHRONOUS)
    
    point = Point(telegraf_measurement) \
        .tag("location", "home") \
        .field("temperature", temperature) \
        .field("humidity", humidity) \
        .time(datetime.utcnow(), "ms")
    
    write_api.write(bucket=influxdb_bucket, record=point)

# Publish data to MQTT topic
def publish_to_mqtt(temperature, humidity):
    client = MQTTClient()
    client.connect(mqtt_broker, mqtt_port)
    
    payload = {
        "temperature": temperature,
        "humidity": humidity
    }
    
    client.publish(mqtt_topic, payload=str(payload))

# Push data to Prometheus
def push_to_prometheus(temperature, humidity):
    registry = CollectorRegistry()
    temperature_metric = Gauge('temperature', 'Temperature metric', registry=registry)
    humidity_metric = Gauge('humidity', 'Humidity metric', registry=registry)
    
    temperature_metric.set(temperature)
    humidity_metric.set(humidity)
    
    push_to_gateway(prometheus_gateway, job=prometheus_job, registry=registry, 
                    grouping_key={'instance': prometheus_instance})

# Create a Grafana dashboard
def create_grafana_dashboard(title, panels):
    headers = {
        "Authorization": f"Bearer {grafana_api_key}",
        "Content-Type": "application/json"
    }

    dashboard_data = {
        "dashboard": {
            "title": title,
            "panels": panels
        },
        "folderId": 0,
        "overwrite": False
    }

    response = requests.post(f"{grafana_url}/api/dashboards/db", headers=headers, json=dashboard_data)
    if response.status_code == 200:
        print("Grafana dashboard created successfully!")
    else:
        print("Failed to create Grafana dashboard.")


    dashboard_title = "Sensor Data Dashboard"
    panels = [
    [    {
            "id": 1,
            "type": "graph",
            "title": "Temperature",
            "datasource": "InfluxDB",
            "targets": [
                {
                    "measurement": telegraf_measurement,
                    "field": "temperature",
                    "groupBy": [],
                    "type": "timeseries"
                }
            ]
        },
        {
            "id": 2,
            "type": "graph",
            "title": "Humidity",
            "datasource": "InfluxDB",
            "targets[
                {
                    "measurement": telegraf_measurement,
                    "field": "humidity",
                    "groupBy": [],
                    "type": "timeseries"
                }
            ]
        }
    ]

    create_grafana_dashboard(dashboard_title, panels)

if __name__ == "__main__":
    main()


# Example usage
def main():
    while True:
        # Generate random temperature and humidity values
        temperature = random.uniform(20, 30)
        humidity = random.uniform(40, 60)
        
        # Write data to InfluxDB
        write_to_influxdb(temperature, humidity)
        
        # Publish data to MQTT
        publish_to_mqtt(temperature, humidity)
        
        # Push data to Prometheus
        push_to_prometheus(temperature, humidity)
        
        time.sleep(5)  # Wait for 5 seconds before publishing the next data


def push_to_grafana():
    # Grafana API information
    grafana_url = 'http://localhost:3000'  # Replace with your Grafana URL
    api_key = 'YOUR_API_KEY'  # Replace with your Grafana API key

    # Create a Prometheus metric
    registry = CollectorRegistry()
    metric = Gauge('my_custom_metric', 'Description of my metric', registry=registry)

    while not shutting_down:
        metric.set(random.randint(0, 100))
        push_to_gateway(grafana_url, job='my_job', registry=registry, basic_auth=(api_key, ''))
        time.sleep(5)

if __name__ == "__main__":
    # Initialize instruments
    initialize_instruments()

    # Start threads for instrument measurements
    tti_measurement_thread = threading.Thread(target=read_tti_func)
    tti_measurement_thread.start()

    keithley_measurement_thread = threading.Thread(target=read_keithley_func)
    keithley_measurement_thread.start()

    # Start writing to InfluxDB
    influxdb_thread = threading.Thread(target=write_to_influxdb)
    influxdb_thread.start()

    # Start pushing to Grafana
    grafana_thread = threading.Thread(target=push_to_grafana)
    grafana_thread.start()

    # Run the Flask app
    app.run(host='0.0.0.0', port=8080)

    # Join all threads on shutdown
    tti_measurement_thread.join()
    keithley_measurement_thread.join()
    influxdb_thread.join()
    grafana_thread.join()





